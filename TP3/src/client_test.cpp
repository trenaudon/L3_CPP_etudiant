#include "client.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

TEST(GroupClient, constructor_plus_getters)  {
    Client client(1, "test");
    CHECK_EQUAL(1, client.getId());
    CHECK_EQUAL("test", client.getNom());
}

