#include "produit.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, constructor_plus_getters)  {
    Produit produit(1, "test");
    CHECK_EQUAL(1, produit.getId());
    CHECK_EQUAL("test", produit.getDescription());
}
