#include "magasin.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin) { };

TEST(GroupMagasin, constructor_plus_getters)  {
    Magasin magasin;
    CHECK_EQUAL(0, magasin.nbClients());
    CHECK_EQUAL(0, magasin.nbProduits());
}

TEST(GroupMagasin, ajouterClient)  {
    Magasin magasin;
    magasin.ajouterClient("jean");
    CHECK_EQUAL(1, magasin.nbClients());
}

TEST(GroupMagasin, supprimerClient)  {
    Magasin magasin;
    magasin.ajouterClient("jean");
    CHECK_EQUAL(1, magasin.nbClients());
    magasin.supprimerClient(0);
    CHECK_EQUAL(0, magasin.nbClients());

    magasin.ajouterClient("a");
    magasin.ajouterClient("b");
    magasin.ajouterClient("c");
    magasin.ajouterClient("d");
    magasin.ajouterClient("e");
    magasin.ajouterClient("f");

    CHECK_EQUAL(6, magasin.nbClients());

    magasin.supprimerClient(3);

    CHECK_THROWS(std::string, magasin.supprimerClient(99999));

}

TEST(GroupMagasin, ajouterProduit)  {
    Magasin magasin;
    magasin.ajouterProduit("jean");
    CHECK_EQUAL(1, magasin.nbProduits());
}

TEST(GroupMagasin, supprimerProduit)  {
    Magasin magasin;
    magasin.ajouterProduit("jean");
    CHECK_EQUAL(1, magasin.nbProduits());
    magasin.supprimerProduit(0);
    CHECK_EQUAL(0, magasin.nbProduits());

    magasin.ajouterProduit("a");
    magasin.ajouterProduit("b");
    magasin.ajouterProduit("c");
    magasin.ajouterProduit("d");
    magasin.ajouterProduit("e");
    magasin.ajouterProduit("f");

    CHECK_EQUAL(6, magasin.nbProduits());

    magasin.supprimerProduit(3);

    CHECK_THROWS(std::string, magasin.supprimerProduit(99999));

}

TEST(GroupMagasin, ajouterLocation)  {
    Magasin magasin;
    magasin.ajouterClient("Jean");
    magasin.ajouterProduit("Pain");
    magasin.ajouterLocation(0, 0);

    CHECK_EQUAL(1, magasin.nbLocations());
    CHECK_THROWS(std::string, magasin.ajouterLocation(0, 0));

    magasin.supprimerLocation(0, 0);
}

TEST(GroupMagasin, supprimerLocation)  {
    Magasin magasin;
    magasin.ajouterClient("Jean");
    magasin.ajouterProduit("Pain");
    magasin.ajouterLocation(0, 0);

    magasin.supprimerLocation(0, 0);

    CHECK_EQUAL(0, magasin.nbLocations());
    CHECK_THROWS(std::string, magasin.supprimerLocation(0, 0));

}

TEST(GroupMagasin, trouverClientDansLocation)  {
    Magasin magasin;
    magasin.ajouterClient("Jean");
    magasin.ajouterProduit("Pain");
    magasin.ajouterLocation(0, 0);

    CHECK_TRUE(magasin.trouverClientDansLocation(0));
    CHECK_FALSE(magasin.trouverClientDansLocation(1));

}

TEST(GroupMagasin, trouverProduitDansLocation)  {
    Magasin magasin;
    magasin.ajouterClient("Jean");
    magasin.ajouterProduit("Pain");
    magasin.ajouterLocation(0, 0);

    CHECK_TRUE(magasin.trouverProduitDansLocation(0));
    CHECK_FALSE(magasin.trouverProduitDansLocation(1));

}
