#include "magasin.h"

Magasin::Magasin():
    _idCourantClient(0), _idCourantProduit(0)
{}

int Magasin::nbClients() const
{
    return static_cast<int>(_clients.size());
}

void Magasin::ajouterClient(const std::string &nom)
{
    Client client(_idCourantClient, nom);
    _clients.push_back(client);
    _idCourantClient++;
}

void Magasin::afficherClients() const
{
    for(Client client : _clients){
        client.afficherClient();
    }
}

void Magasin::supprimerClient(int idClient)
{
    if(idClient < static_cast<int>(_clients.size())){

        std::swap(_clients[static_cast<unsigned int>(idClient)], _clients.back());
        _clients.pop_back();

        for(int i = idClient+1; i < static_cast<int>(_clients.size()); i++ ){
            std::swap(_clients[static_cast<unsigned int>(i-1)], _clients[static_cast<unsigned int>(i)]);
        }
    }else
        throw std::string("erreur: ce client n'existe pas");
}

int Magasin::nbProduits() const
{
    return static_cast<int>(_produits.size());
}

void Magasin::ajouterProduit(const std::string &nom)
{
    Produit produit(_idCourantProduit, nom);
    _produits.push_back(produit);
    _idCourantProduit++;
}

void Magasin::afficherProduits() const
{
    for(Produit produit : _produits){
        produit.afficherProduit();
    }
}

void Magasin::supprimerProduit(int idProduit)
{
    if(idProduit < static_cast<int>( _produits.size()) && idProduit >= 0){

        std::swap(_produits[static_cast<unsigned int>(idProduit)], _produits.back());
        _produits.pop_back();

        for(int i = idProduit+1; i < static_cast<int>(_produits.size()); i++ ){
            std::swap(_produits[static_cast<unsigned int>(i-1)], _produits[static_cast<unsigned int>(i)]);
        }
    }else
        throw std::string("erreur: ce produit n'existe pas");
}

int Magasin::nbLocations() const
{
    return static_cast<int>(_locations.size());
}

void Magasin::ajouterLocation(int idClient, int idProduit)
{
    Location location;
    location._idClient = idClient;
    location._idProduit = idProduit;
    if(!trouverClientDansLocation(idClient) || !trouverProduitDansLocation(idProduit))
        _locations.push_back(location);
    else
        throw std::string("erreur: cette location existe déjà");
}

void Magasin::afficherLocations() const
{
    for(Location location : _locations){
        location.afficherLocation();
    }
}

void Magasin::supprimerLocation(int idClient, int idProduit)
{
    bool locationFound = false;
    unsigned int position = 0;

    if(trouverClientDansLocation(idClient) && trouverProduitDansLocation(idProduit)){

        while (position < _locations.size() && !locationFound) {
            if(_locations[position]._idClient == idClient && _locations[position]._idProduit == idProduit )
                locationFound = true;
            else
                position++;
        }

        if(locationFound){
            std::swap(_locations[position], _locations.back());
            _locations.pop_back();
        }
        for(unsigned i = position+1; i < _produits.size(); i++ ){
            std::swap(_produits[i-1], _produits[i]);
        }
    }

    if(!locationFound)
        throw std::string("erreur: cette location n'existe pas");

}

bool Magasin::trouverClientDansLocation(int idClient) const
{
    bool clientFound = false;
    unsigned int position = 0;

    if(idClient < static_cast<int>(_locations.size()) && idClient >= 0 ){

        while (position < _locations.size() && !clientFound) {
            if(_locations[position]._idClient == idClient)
                clientFound = true;
            else
                position++;
        }
    }

    return clientFound;
}

std::vector<int> Magasin::calculerClientsLibres() const
{
    std::vector<int> idClientsLibres;

    for(Client client : _clients){
        if(!trouverClientDansLocation(client.getId())){
            idClientsLibres.push_back(client.getId());
        }
    }

    return idClientsLibres;
}

bool Magasin::trouverProduitDansLocation(int idProduit) const
{
    bool produitFound = false;
    unsigned int position = 0;

    if(idProduit < static_cast<int>(_locations.size()) && idProduit >= 0 ){

        while (position < _locations.size() && !produitFound) {
            if(_locations[position]._idProduit == idProduit)
                produitFound = true;
            else
                position++;
        }
    }

    return produitFound;
}

std::vector<int> Magasin::calculerProduitsLibres() const
{
    std::vector<int> idProduitLibres;

    for(Produit produit : _produits){
        if(!trouverProduitDansLocation(produit.getId())){
            idProduitLibres.push_back(produit.getId());
        }
    }

    return idProduitLibres;
}
