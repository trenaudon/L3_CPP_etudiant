#include <iostream>
#include "location.h"
#include "magasin.h"

int main() {

    Location location;
    location._idClient = 0;
    location._idProduit = 2;
    location.afficherLocation();

    Magasin magasin;
    magasin.ajouterClient("a");
    magasin.ajouterClient("b");
    magasin.ajouterClient("c");
    magasin.ajouterClient("d");
    magasin.ajouterClient("e");
    std::cout << magasin.nbClients() << std::endl;
    magasin.afficherClients();
    magasin.supprimerClient(1);
    std::cout << magasin.nbClients() << std::endl;
    magasin.afficherClients();
    magasin.ajouterClient("f");
    std::cout << magasin.nbClients() << std::endl;
    magasin.afficherClients();

    magasin.ajouterProduit("a");
    magasin.ajouterProduit("b");
    magasin.ajouterProduit("c");
    magasin.ajouterProduit("d");
    magasin.ajouterProduit("e");
    std::cout << magasin.nbProduits() << std::endl;
    magasin.afficherProduits();
    magasin.supprimerProduit(1);
    std::cout << magasin.nbProduits() << std::endl;
    magasin.afficherProduits();
    magasin.ajouterProduit("f");
    std::cout << magasin.nbProduits() << std::endl;
    magasin.afficherProduits();

    return 0;
}

