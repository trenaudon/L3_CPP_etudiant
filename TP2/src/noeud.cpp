#include "noeud.h"

Noeud::Noeud(int valeur):
    _valeur(valeur), _suivant(nullptr)
{}

Noeud::Noeud(int valeur, Noeud *suivant):
    _valeur(valeur), _suivant(suivant)
{}
