#include "liste.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, constructor)  {
    Liste liste;
    POINTERS_EQUAL(liste._tete, nullptr);
}

TEST(GroupListe, ajouterDevant)  {
    Liste liste;
    liste.ajouterDevant(1);
    CHECK_FALSE(liste._tete == nullptr);
    CHECK_EQUAL(1, liste._tete->_valeur);
    POINTERS_EQUAL(nullptr, liste._tete->_suivant);

    liste.ajouterDevant(2);
    CHECK_FALSE(liste._tete == nullptr);
    CHECK_EQUAL(2, liste._tete->_valeur);
    CHECK_FALSE(liste._tete->_suivant == nullptr);
    CHECK_EQUAL(1, liste._tete->_suivant->_valeur);
    POINTERS_EQUAL(nullptr, liste._tete->_suivant->_suivant);
}


TEST(GroupListe, getTaille)  {
    Liste liste;
    liste.ajouterDevant(1);
    CHECK_EQUAL(liste.getTaille(), 1);
    liste.ajouterDevant(2);
    liste.ajouterDevant(3);
    CHECK_EQUAL(liste.getTaille(), 3);
}

TEST(GroupListe, getElement)  {
    Liste liste;
    liste.ajouterDevant(1);
    CHECK_EQUAL(liste.getElement(0), 1);
    liste.ajouterDevant(2);
    liste.ajouterDevant(3);
    CHECK_EQUAL(liste.getElement(1), 2);
    CHECK_EQUAL(liste.getElement(2), 1);
}
