#ifndef LISTE_H
#define LISTE_H

#include "noeud.h"

struct Liste
{
    Noeud* _tete;
    Liste();
    ~Liste();
    void ajouterDevant(int valeur);
    int getTaille() const;
    int getElement(int indice) const;
};

#endif // LISTE_H
