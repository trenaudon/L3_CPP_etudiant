#include "liste.h"

Liste::Liste():
    _tete(nullptr)
{}

Liste::~Liste()
{
    Noeud* current = _tete;
    Noeud* toDelete = _tete;

    if(current != nullptr){
        while (current->_suivant != nullptr) {
            current = current->_suivant;
            delete toDelete;
            toDelete = current;
        }
        delete toDelete;
    }

}

void Liste::ajouterDevant(int valeur)
{
    if(_tete == nullptr){
        _tete = new Noeud(valeur);
    }else {
        Noeud* tmp = _tete;
        _tete = new Noeud(valeur);
        _tete->_suivant = tmp;
    }
}

int Liste::getTaille() const
{
    Noeud* current = _tete;
    int size = 1;

    while (current->_suivant != nullptr) {
        current = current->_suivant;
        size ++;
    }

    return size;
}

int Liste::getElement(int indice) const
{
    Noeud* current = _tete;
    int size = 0;
    int element = -1;
    bool found = false;

    if(!indice){
        element = _tete->_valeur;
        found = true;
    }

    while (current->_suivant != nullptr && size != indice) {
        current = current->_suivant;
        size ++;
    }

    if(size == indice)
        element = current->_valeur;

    return element;

}
