#include <iostream>
#include "liste.h"

int main()
{
    Liste liste;
    liste.ajouterDevant(13);
    liste.ajouterDevant(37);

    for (int i = 0; i <liste.getTaille(); i++) {
       std::cout << liste.getElement(i) << std::endl;
    }
    return 0;
}
