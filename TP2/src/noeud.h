#ifndef NOEUD_H
#define NOEUD_H


struct Noeud
{
    int _valeur;
    Noeud* _suivant;
    Noeud(int valeur);
    Noeud(int valeur, Noeud* suivant);
};

#endif // NOEUD_H
