#include "noeud.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupNoeud) { };

TEST(GroupNoeud, constructor_with_one_arg)  {
    Noeud node(1);
    CHECK_EQUAL(1, node._valeur);
    POINTERS_EQUAL(nullptr, node._suivant);
}

TEST(GroupNoeud, constructor_with_two_arg)  {
    Noeud node(1);
    Noeud node1(2, &node);

    CHECK_EQUAL(1, node._valeur);
    POINTERS_EQUAL(nullptr, node._suivant);
    CHECK_EQUAL(2, node1._valeur);
    POINTERS_EQUAL(&node, node1._suivant);
    CHECK_EQUAL(1, node1._suivant->_valeur);
    POINTERS_EQUAL(nullptr, node1._suivant->_suivant);
}

