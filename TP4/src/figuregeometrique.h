#ifndef FIGUREGEOMETRIQUE_H
#define FIGUREGEOMETRIQUE_H
#include "couleur.h"


class FigureGeometrique
{
protected:
    Couleur _couleur;
public:
    virtual ~FigureGeometrique();
    FigureGeometrique(const Couleur &couleur);
    const Couleur& getCouleur() const;
    virtual void afficher() const = 0;
};

#endif // FIGUREGEOMETRIQUE_H
