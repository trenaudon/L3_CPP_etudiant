#ifndef POLYGONEREGULIER_H
#define POLYGONEREGULIER_H
#include "couleur.h"
#include "point.h"
#include "figuregeometrique.h"

class PolygoneRegulier: public FigureGeometrique
{
private:
    int _nbPoints;
    Point* _points;
public:
    PolygoneRegulier(const Couleur& couleur, const Point& centre, int rayon, int nbCotes);
    ~PolygoneRegulier();
    void afficher() const;
    int getNbPoints() const;
    const Point& getPoint(int indice) const;
};

#endif // POLYGONEREGULIER_H
