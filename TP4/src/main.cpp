#include "ligne.h"
#include "couleur.h"
#include "point.h"
#include "polygoneregulier.h"
#include "figuregeometrique.h"
#include <vector>

int main() {
    Ligne ligne(Couleur{1, 0, 0}, Point{0, 0}, Point{100, 200});
    PolygoneRegulier polygoneRegulier(Couleur{0, 1, 0}, Point{100, 200}, 50, 5);
    std::vector<FigureGeometrique*> figures;
    figures.push_back(&ligne);
    figures.push_back(&polygoneRegulier);

    ligne.afficher();
    polygoneRegulier.afficher();

    for (unsigned i = 0; i < figures.size(); i++) {
       figures[i]->afficher();
    }
}

