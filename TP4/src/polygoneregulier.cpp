#include "polygoneregulier.h"
#include "math.h"
#include <iostream>
#include <vector>

PolygoneRegulier::PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes):
    FigureGeometrique (couleur), _nbPoints(nbCotes)
{
    int tour = 360;
    int angleParPoints = tour / nbCotes;

    _points = new Point[static_cast<unsigned int>(nbCotes)];

    for(int i = 0, j = 0 ; i < tour; i += angleParPoints, j++){
        _points[j]._x = static_cast<int>(cos( (2*M_PI * i) / (tour)) * rayon + centre._x);
        _points[j]._y = static_cast<int>(sin( (2*M_PI * i) / (tour)) * rayon + centre._y);
    }
}

PolygoneRegulier::~PolygoneRegulier()
{
    delete[] _points;
}

void PolygoneRegulier::afficher() const
{
    std::cout << "PolygonRegulier " << getCouleur()._r << "_" << _couleur._g << "_" << getCouleur()._b;

    for(int i = 0; i < _nbPoints; i++){
        std::cout << " " << _points[i]._x << "_" << _points[i]._y;
    }

    std::cout << std::endl;
}

int PolygoneRegulier::getNbPoints() const
{
    return _nbPoints;
}

const Point &PolygoneRegulier::getPoint(int indice) const
{
    return _points[indice];
}
