#include <iostream>
#include "Fibonacci.h"

int fibonacciIteratif(int n){
	int valueN;
	
	if(n <= 0)
		valueN = 0;
	else if(n > 0 && n <= 2)
		valueN = 1;
	else{
		
		int valueNmoins2 = 1;
		int valueNmoins1 = 1;
		
		for (int i = 2; i < n; i++){
			
			int tmp = valueNmoins2;
			valueNmoins2 = valueNmoins1;
			valueNmoins1 = valueNmoins1 + tmp;
			
		}
		valueN = valueNmoins1;
		
	}
	
	return valueN;
}

int fibonacciRecursif(int n)
{
    if (n <= 0)
        return 0;
    if (n <= 2)
        return 1;
    else
        return fibonacciRecursif(n - 1) + fibonacciRecursif(n - 2);
}
