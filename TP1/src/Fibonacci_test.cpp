#include "Fibonacci.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacciIteratif) { };

TEST(GroupFibonacciIteratif, Fibonacci_test1) {
    CHECK_EQUAL(fibonacciIteratif(1), 1);
}

TEST(GroupFibonacciIteratif, Fibonacci_test2) {
    CHECK_EQUAL(fibonacciIteratif(2), 1);
}

TEST(GroupFibonacciIteratif, Fibonacci_test3) {
    CHECK_EQUAL(fibonacciIteratif(3), 2);
}

TEST(GroupFibonacciIteratif, Fibonacci_test4) {
    CHECK_EQUAL(fibonacciIteratif(4), 3);
}

TEST(GroupFibonacciIteratif, Fibonacci_test5) {
    CHECK_EQUAL(fibonacciIteratif(5), 5);
}


TEST_GROUP(GroupFibonacciRecursif) { };

TEST(GroupFibonacciRecursif, Fibonacci_test1) {
    CHECK_EQUAL(fibonacciIteratif(1), 1);
}

TEST(GroupFibonacciRecursif, Fibonacci_test2) {
    CHECK_EQUAL(fibonacciIteratif(2), 1);
}

TEST(GroupFibonacciRecursif, Fibonacci_test3) {
    CHECK_EQUAL(fibonacciIteratif(3), 2);
}

TEST(GroupFibonacciRecursif, Fibonacci_test4) {
    CHECK_EQUAL(fibonacciIteratif(4), 3);
}

TEST(GroupFibonacciRecursif, Fibonacci_test5) {
    CHECK_EQUAL(fibonacciIteratif(5), 5);
}
