#include "vecteur3.h"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(groupVecteur) { };

TEST(groupVecteur, Vecteur3_norme) {
    Vecteur3 vecteur(2, 3, 6);
    CHECK_EQUAL(vecteur.norme(), 7);
}

TEST(groupVecteur, Vecteur3_ProduitScalaire) {
    Vecteur3 vecteur1(2, 3, 6);
    Vecteur3 vecteur2(4, 5, 7);
    CHECK_EQUAL(Vecteur3::produitScalaire(vecteur1, vecteur2), 65);
}

TEST(groupVecteur, Vecteur3_Addition) {
    Vecteur3 vecteur1(2, 3, 6);
    Vecteur3 vecteur2(4, 5, 7);
    CHECK_EQUAL(Vecteur3::addition(vecteur1, vecteur2), 27);
}
