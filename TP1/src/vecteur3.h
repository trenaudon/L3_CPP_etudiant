#ifndef VECTEUR3_H
#define VECTEUR3_H

class Vecteur3
{
private:
    float x;
    float y;
    float z;

public:
    Vecteur3(float x, float y, float z);

    float getX();
    float getY();
    float getZ();

    void afficher();
    float norme();
    static float produitScalaire(Vecteur3 vecteur1, Vecteur3 vecteur2);
    static float addition(Vecteur3 vecteur1, Vecteur3 vecteur2);
};

#endif // VECTEUR3_H
