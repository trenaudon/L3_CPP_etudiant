#include "vecteur3.h"
#include <iostream>

void afficher(float x, float y, float z){
    std::cout << "("
              << x
              << ", "
              << y
              << ", "
              << z
              << ")"
              << std::endl;
}

int main()
{
    Vecteur3 vecteur(2, 3, 6);

    afficher(vecteur.getX(), vecteur.getY(), vecteur.getZ());

    vecteur.afficher();

    std::cout << vecteur.norme() << std::endl;

    return 0;
}
