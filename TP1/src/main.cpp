#include <iostream>
#include "Fibonacci.h"

int main(){
	std::cout << "Hello, world !" << std::endl;
	std::cout << fibonacciIteratif(2) << std::endl;
	std::cout << fibonacciIteratif(4) << std::endl;
	std::cout << fibonacciIteratif(10) << std::endl;
	std::cout << fibonacciRecursif(2) << std::endl;
	std::cout << fibonacciRecursif(4) << std::endl;
	std::cout << fibonacciRecursif(10) << std::endl;
}
