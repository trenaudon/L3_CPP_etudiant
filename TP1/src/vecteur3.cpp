#include "vecteur3.h"
#include <iostream>
#include <math.h>

Vecteur3::Vecteur3(float x, float y, float z):
    x(x),
    y(y),
    z(z)
{}

float Vecteur3::getX()
{
    return this->x;
}

float Vecteur3::getY()
{
    return this->y;
}

float Vecteur3::getZ()
{
    return this->z;
}

void Vecteur3::afficher()
{
    std::cout << "("
              << this->x
              << ", "
              << this->y
              << ", "
              << this->z
              << ")"
              << std::endl;
}

float Vecteur3::norme()
{
    return static_cast<float>(sqrt((pow(this->x, 2)
                                    + pow(this->y, 2)
                                    + pow(this->z, 2)))
                              );
}

float Vecteur3::produitScalaire(Vecteur3 vecteur1, Vecteur3 vecteur2)
{
    return (vecteur1.x * vecteur2.x) + (vecteur1.y * vecteur2.y) + (vecteur1.z * vecteur2.z);
}

float Vecteur3::addition(Vecteur3 vecteur1, Vecteur3 vecteur2)
{
    return (vecteur1.x + vecteur2.x) + (vecteur1.y + vecteur2.y) + (vecteur1.z + vecteur2.z);
}
