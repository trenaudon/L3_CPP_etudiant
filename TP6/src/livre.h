#ifndef LIVRE_H
#define LIVRE_H

#include <string>

class Livre
{
private:
    std::string _titre;
    std::string _auteur;
    int _annee;

public:
    Livre();
    Livre(const std::string &titre, const std::string &auteur, const int &annee);

    friend std::istream& operator>>(std::istream &input, Livre &livre);
    friend std::ostream& operator<<(std::ostream &input, Livre &livre);

    const std::string &getTitre() const;
    const std::string &getAuteur() const;
    const int &getAnnee() const;
};

bool operator== (const Livre livre1, const Livre livre2);
bool operator< (const Livre &livre1, const Livre &livre2);


#endif // LIVRE_H
