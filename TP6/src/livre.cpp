#include "livre.h"

Livre::Livre()
{

}

Livre::Livre(const std::string &titre, const std::string &auteur, const int &annee):
    _annee(annee)
{

    if(auteur.find(';') != std::string::npos)
        throw std::string("erreur : auteur non valide (';' non autorisé)");

    else if(auteur.find('\n') != std::string::npos)
        throw std::string("erreur : auteur non valide ('\n' non autorisé)");

    else
        _auteur = auteur;


    if(titre.find(';') != std::string::npos)
        throw std::string("erreur : titre non valide (';' non autorisé)");

    else if(titre.find('\n') != std::string::npos)
        throw std::string("erreur : titre non valide ('\n' non autorisé)");

    else
        _titre = titre;

}

const std::string &Livre::getTitre() const
{
    return _titre;
}

const std::string &Livre::getAuteur() const
{
    return _auteur;
}

const int &Livre::getAnnee() const
{
    return _annee;
}

std::istream &operator>>(std::istream &input, Livre &livre)
{
    std::string stringStream;
    std::string annee;
    input >> stringStream;

    livre._titre = stringStream.substr(0,stringStream.find(';'));
    livre._auteur = stringStream.substr(stringStream.find(';')+1);
    livre._auteur = livre._auteur.substr(0, livre._auteur.find(';'));
    livre._annee = std::stoi(stringStream.substr(stringStream.rfind(';')+1));

    return input;
}

std::ostream &operator<<(std::ostream &output, Livre &livre)
{
    std::string stringStream;
    stringStream += livre._titre;
    stringStream += ';';
    stringStream += livre._auteur;
    stringStream += ';';
    stringStream += std::to_string(livre._annee);

    output << stringStream;

    return output;
}

bool operator== (const Livre livre1, const Livre livre2)
{
    return (livre1.getAnnee() == livre2.getAnnee() && livre1.getTitre() == livre2.getTitre()
            && livre1.getAuteur() == livre2.getAuteur());
}

bool operator< (const Livre &livre1, const Livre &livre2)
{
    if(livre1.getAuteur() < livre2.getAuteur())
        return true;

    else if(livre1.getAuteur() == livre2.getAuteur()){

        if(livre1.getTitre() < livre2.getTitre())
            return true;
        else
            return false;

    }else
        return false;
}
