#ifndef BIBLIOTHEQUE_H
#define BIBLIOTHEQUE_H

#include <string>
#include <vector>
#include "livre.h"

class Bibliotheque: public std::vector<Livre>
{
public:
    Bibliotheque();
    void afficher() const;
    void trierParAuteurEtTitre();
    void trierParAnnee();
    void lireFichier(const std::string &nomFichier);
    void ecrireFichier(const std::string &nomFichier);
};

#endif // BIBLIOTHEQUE_H
