#include "bibliotheque.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <functional>
#include "livre.h"

Bibliotheque::Bibliotheque()
{

}

void Bibliotheque::afficher() const
{
    for(Livre livre : *this){
        std::cout << "Titre : " << livre.getTitre() << std::endl;
        std::cout << "Auteur : " << livre.getAuteur() << std::endl;
        std::cout << "Annee : " << livre.getAnnee() << std::endl;
        std::cout << std::endl;
    }
}

void Bibliotheque::trierParAuteurEtTitre()
{
    std::sort(begin(), end(), [](Livre livre1, Livre livre2){
        return livre1<livre2;
    });

}

void Bibliotheque::trierParAnnee()
{
    std::sort(begin(), end(), [](Livre livre1, Livre livre2){
        return livre1.getAnnee()<livre2.getAnnee();
    });
}

void Bibliotheque::lireFichier(const std::string &nomFichier)
{
    std::ifstream infile;
    Livre livre;

    infile.open(nomFichier);

    if(infile.good()){
        while (!infile.eof()) {
            infile >> livre;
            this->push_back(livre);
        }
        infile.close();
    }else
        throw std::string("erreur : lecture du fichier impossible");




}

void Bibliotheque::ecrireFichier(const std::string &nomFichier)
{
    std::ofstream outfile;
    outfile.open(nomFichier);

    for (int i = 0; i<static_cast<int>(this->size()); i++) {

        outfile << this->at(static_cast<size_type>(i));

        if(i < static_cast<int>(this->size()-1))
            outfile << std::endl;
    }

    outfile.close();
}
