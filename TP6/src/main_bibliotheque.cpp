#include <iostream>
#include "bibliotheque.h"
#include "livre.h"

int main() {
    Bibliotheque bibliotheque;
    bibliotheque.push_back(Livre("t1", "a1", 1));
    bibliotheque.push_back(Livre("t2", "a2", 3));
    bibliotheque.afficher();

    Bibliotheque b2;
    b2.lireFichier("bibliotheque_fichier_tmp.txt");
    b2.afficher();
    return 0;
}

