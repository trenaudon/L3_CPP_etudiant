#ifndef FIGUREGEOMETRIQUE_H
#define FIGUREGEOMETRIQUE_H
#include "couleur.h"
#include <gtkmm.h>

class FigureGeometrique
{
protected:
    Couleur _couleur;
public:
    virtual ~FigureGeometrique();
    FigureGeometrique(const Couleur &couleur);
    const Couleur& getCouleur() const;
    virtual void afficher(const Cairo::RefPtr<Cairo::Context> &context) const = 0;
};

#endif // FIGUREGEOMETRIQUE_H
