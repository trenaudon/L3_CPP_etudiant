#include "ligne.h"
#include "couleur.h"
#include "point.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLigne) { };

TEST(GroupLigne, Constructeur_Getters)  {
    Ligne ligne(Couleur{253, 254, 255}, Point{0, 1}, Point{1, 2});

    CHECK_EQUAL(0, ligne.getP0()._x);
    CHECK_EQUAL(1, ligne.getP0()._y);
    CHECK_EQUAL(1, ligne.getP1()._x);
    CHECK_EQUAL(2, ligne.getP1()._y);
}
