#ifndef VIEWERFIGURES_H
#define VIEWERFIGURES_H

#include <gtkmm.h>
#include <string>
#include "zonedessin.h"

class ViewerFigures
{
private:
    Gtk::Main _kit;
    Gtk::Window _window;
    Gtk::Label _label;
    ZoneDessin _zonedessin;

    int _width = 640;
    int _height = 480;

    std::string _title = "Titre";

public:
    ViewerFigures(int argc, char** argv);
    void run();
};

#endif // VIEWERFIGURES_H
