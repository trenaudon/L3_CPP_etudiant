#include "zonedessin.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

bool ZoneDessin::on_expose_event(GdkEventExpose *event)
{
    Cairo::RefPtr<Cairo::Context> context = this->get_window()->create_cairo_context();

    for (FigureGeometrique* figure : _figures){
        context->save();
        figure->afficher(context);
        context->restore();
    }
    return true;
}

bool ZoneDessin::gererClic(GdkEventButton *event)
{
    if(event->type == GDK_BUTTON_PRESS && event->button == 1){
        int rayon = rand() % 100+1;
        int nbCotes = rand() % 20+3;
        _figures.push_back(new PolygoneRegulier(Couleur{0, 1, 0}, Point{static_cast<int>(event->x), static_cast<int>(event->y)}, rayon, nbCotes));

    }

    else if(event->type == GDK_BUTTON_PRESS && event->button == 3){

        if(_figures.size() > 0)
            _figures.pop_back();

    }

    get_window()->invalidate(true);
    return true;
}

ZoneDessin::ZoneDessin()
{
//    _context = get_window()->create_cairo_context();
    this->signal_button_press_event().connect(sigc::mem_fun(*this, &ZoneDessin::gererClic));
    this->add_events(Gdk::BUTTON_PRESS_MASK);
    srand(time(nullptr));
}

ZoneDessin::~ZoneDessin()
{
    for (FigureGeometrique* figure: _figures) {
        delete figure;
    }
}
