#include "viewerfigures.h"

ViewerFigures::ViewerFigures(int argc, char **argv):
    _kit(Gtk::Main(argc, argv)), _label(" Hello world ! ")
{
    _window.set_title(_title);
    _window.set_default_size(_width, _height);
    _window.add(_zonedessin);
    _window.show_all();
}

void ViewerFigures::run()
{
    _kit.run(_window);
}
