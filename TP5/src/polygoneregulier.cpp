#include "polygoneregulier.h"
#include "math.h"
#include <iostream>
#include <vector>

PolygoneRegulier::PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes):
    FigureGeometrique (couleur), _nbPoints(nbCotes)
{
    int tour = 360;
    double i = 0;
    double angleParPoints = 360.0 / nbCotes;

    _points = new Point[static_cast<unsigned int>(nbCotes)];

    for(int j = 0 ; j < nbCotes; i += angleParPoints, j++){
        _points[j]._x = static_cast<int>(cos( (2*M_PI * i) / (tour)) * rayon + centre._x);
        _points[j]._y = static_cast<int>(sin( (2*M_PI * i) / (tour)) * rayon + centre._y);
    }
}

PolygoneRegulier::~PolygoneRegulier()
{
    delete[] _points;
}

void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> & context) const
{    

//    std::cout << "PolygonRegulier " << getCouleur()._r << "_" << _couleur._g << "_" << getCouleur()._b;
    context->set_source_rgb(_couleur._r, _couleur._g, _couleur._b);
    context->set_line_width(1.0);
    context->move_to(_points[0]._x, _points[0]._y);

    for(int i = 1; i < _nbPoints; i++){
        context->line_to(_points[i]._x, _points[i]._y);
//        std::cout << " " << _points[i]._x << "_" << _points[i]._y;
    }

    context->line_to(_points[0]._x, _points[0]._y);
    context->stroke();

//    std::cout << std::endl;


}

int PolygoneRegulier::getNbPoints() const
{
    return _nbPoints;
}

const Point &PolygoneRegulier::getPoint(int indice) const
{
    return _points[indice];
}
