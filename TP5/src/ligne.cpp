#include "ligne.h"
#include <iostream>

Ligne::Ligne(const Couleur &couleur, const Point &p0, const Point &p1):
    FigureGeometrique(couleur), _p0(p0), _p1(p1)
{}

const Point &Ligne::getP0() const
{
    return _p0;
}

const Point &Ligne::getP1() const
{
    return _p1;
}

void Ligne::afficher(const Cairo::RefPtr<Cairo::Context> & context) const
{
//    std::cout << "Ligne " << getCouleur()._r << "_" << _couleur._g << "_" << getCouleur()._b
//              << " " << getP0()._x << "_" << getP0()._y
//              << " " << getP1()._x << "_" << getP1()._y << std::endl;

    context->set_source_rgb(_couleur._r, _couleur._g, _couleur._b);
    context->set_line_width(10.0);
    context->move_to(getP0()._x, getP0()._y);
    context->line_to(getP1()._x, getP1()._y);
    context->stroke();
}



