#include "polygoneregulier.h"
#include "couleur.h"
#include "point.h"
#include "math.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupPolygoneRegulier) { };

TEST(GroupPolygoneRegulier, Constructeur_Getter)  {
    PolygoneRegulier polygone(Couleur{253, 254, 255}, Point{0, 0}, 1, 4);
    Point Droite{1, 0};
    Point Haut{0, 1};
    Point Gauche{-1, 0};
    Point Bas{0, -1};

    CHECK_EQUAL(4, polygone.getNbPoints());

    CHECK_EQUAL(Droite._x, polygone.getPoint(0)._x);
    CHECK_EQUAL(Droite._y, polygone.getPoint(0)._y);
    CHECK_EQUAL(Haut._x, polygone.getPoint(1)._x);
    CHECK_EQUAL(Haut._y, polygone.getPoint(1)._y);
    CHECK_EQUAL(Gauche._x, polygone.getPoint(2)._x);
    CHECK_EQUAL(Gauche._y, polygone.getPoint(2)._y);
    CHECK_EQUAL(Bas._x, polygone.getPoint(3)._x);
    CHECK_EQUAL(Bas._y, polygone.getPoint(3)._y);
}
