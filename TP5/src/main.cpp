#include "ligne.h"
#include "couleur.h"
#include "point.h"
#include "polygoneregulier.h"
#include "figuregeometrique.h"
#include "viewerfigures.h"
#include "zonedessin.h"

int main(int argc, char ** argv) {
    ViewerFigures viewerfigures(argc, argv);
    viewerfigures.run();
}

