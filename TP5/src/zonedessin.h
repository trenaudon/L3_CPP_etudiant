#ifndef ZONEDESSIN_H
#define ZONEDESSIN_H

#include <gtkmm.h>
#include <vector>

#include "figuregeometrique.h"
#include "polygoneregulier.h"
#include "ligne.h"


class ZoneDessin: public Gtk::DrawingArea
{
private:
    std::vector<FigureGeometrique*> _figures;

protected:
    bool on_expose_event(GdkEventExpose* event);
    bool gererClic(GdkEventButton *event);
public:
    ZoneDessin();
    ~ZoneDessin();

};

#endif // ZONEDESSIN_H
